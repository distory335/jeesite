/**
 * Copyright &copy; 2012-2016 <a href="http://git.oschina.net/wocadi/jeesite">JeeSite</a> All rights reserved.
 */
package com.zscat.oa.dao;

import com.zscat.common.persistence.CrudDao;
import com.zscat.common.persistence.annotation.MyBatisDao;
import com.zscat.oa.entity.OaNotify;

/**
 * 通知通告DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@MyBatisDao
public interface OaNotifyDao extends CrudDao<OaNotify> {
	
	/**
	 * 获取通知数目
	 * @param oaNotify
	 * @return
	 */
	public Long findCount(OaNotify oaNotify);
	
}